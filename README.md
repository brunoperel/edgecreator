EdgeCreator
===========

Un outil graphique permettant de concevoir des tranches de magazines.
EdgeCreator appartient au projet [DucksManager](https://github.com/ducksmanager/DucksManager)

﻿![Structure de EdgeCreator et interaction avec DucksManager](http://www.gliffy.com/go/publish/image/4959615/L.png)


Envie d'en savoir plus ? [Accédez au wiki](https://github.com/ducksmanager/EdgeCreator/wiki)
